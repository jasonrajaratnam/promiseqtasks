from ultralytics import YOLO
import torch
import threading
import cv2
import os

DEVICE = torch.device("cuda")
model1 = YOLO('yolov8n.pt')
model2 = YOLO('yolov8n.pt')
model3 = YOLO('yolov8n.pt')
model4 = YOLO('yolov8n.pt')

# Lock to ensure thread-safe writing to video files
write_lock = threading.Lock()

def run_tracker_in_thread(filename, model, file_index):
    output_video_file = f"output_video_{file_index}.mp4"
    
    video = cv2.VideoCapture(filename)  # Read the video file

    # Define the codec and create a VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    frame_rate = int(video.get(cv2.CAP_PROP_FPS))
    frame_size = (int(video.get(3)), int(video.get(4)))
    
    with write_lock:
        out = cv2.VideoWriter(output_video_file, fourcc, frame_rate, frame_size)
    
    while True:
        ret, frame = video.read()  # Read the video frames

        # Exit the loop if no more frames in the video
        if not ret:
            break

        # Track objects in frames if available
        results = model.track(frame, persist=True, save=True)
        res_plotted = results[0].plot()

        with write_lock:
            # Save the frame to the output video
            out.write(res_plotted)

        key = cv2.waitKey(1)
        if key == ord('q'):
            break

    # Release video sources and close the output video file
    video.release()
    
    with write_lock:
        out.release()

# Define the video files for the trackers
video_files = [
    "data/archive/multi/MOT17-02-DPM-raw.webm",
    "data/archive/multi/MOT17-04-SDP-raw.webm",
    # "archive/multi/MOT17-05-SDP-raw.webm",
    # "archive/multi/MOT17-13-SDP-raw.webm"
]

if __name__ == '__main__':
    # Create separate threads for each video file
    threads = []
    for i, video_file in enumerate(video_files, start=1):
        model = model1 if i == 1 else model2 if i == 2 else model3 if i == 3 else model4
        thread = threading.Thread(target=run_tracker_in_thread, args=(video_file, model, i))
        threads.append(thread)
        thread.start()

    # Wait for the threads to finish
    for thread in threads:
        thread.join()

    cv2.destroyAllWindows()
